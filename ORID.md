# Daily Report (2023/07/20)
# O (Objective): 
## 1. What did we learn today?
### We learned SQL Basic and Spring Data JPA today.

## 2. What activities did you do? 
### I had a code review with my team members, played a game and used JPA to operate the database and write the Integration testing of the interface.

## 3. What scenes have impressed you?
### I have been impressed by the code review with my team members.

---------------------

# R (Reflective): 
## Please use one word to express your feelings about today's class.
---------------------
### Relaxed.

# I (Interpretive):
## What do you think about this? What was the most meaningful aspect of this activity?
---------------------
### I think the most meaningful aspect of this activity is showing the excellent code of Gary, because I came to understand a new concept - test coverage and I am surprised by his 100% test coverage, and I think we all need to learn from him.


# D (Decisional): 
## Where do you most want to apply what you have learned today? What changes will you make?
---------------------
### I will use Java stream instead of loop and try to reduce the use of if else statements as much as possible when I write the functions, and I will use JPA to operate the database.
### I will develop a good coding habit of committing step by step. And I will strengthen communication with group members when doing group assignments in order to set aside time to rehearse the content of the speech in advance and present better performance results.



