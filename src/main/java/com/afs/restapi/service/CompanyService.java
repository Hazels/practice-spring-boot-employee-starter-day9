package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public CompanyRepository getCompaniesRepository() {
        return companyRepository;
    }

    public EmployeeRepository getEmployeesRepository() {
        return employeeRepository;
    }

    public List<Company> findAll() {
        return getCompaniesRepository().findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return getCompaniesRepository().findAll(PageRequest.of(page - 1, size)).getContent();
    }

    public Company findById(Long id) {
        Company company = getCompaniesRepository().findById(id).orElseThrow(CompanyNotFoundException::new);
        List<Employee> employees = getEmployeesRepository().findByCompanyId(company.getId());
        company.setEmployees(employees);
        return company;
    }

    public void update(Long id, Company company) {
        Optional<Company> optionalCompany = getCompaniesRepository().findById(id);
        optionalCompany.ifPresent(previousCompany -> previousCompany.setName(company.getName()));
        companyRepository.save(optionalCompany.get());
    }

    public Company create(Company company) {
        return getCompaniesRepository().save(company);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return getEmployeesRepository().findByCompanyId(id);
    }

    public void delete(Long id) {
        companyRepository.deleteById(id);
    }
}
