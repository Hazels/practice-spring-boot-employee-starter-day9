package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public EmployeeRepository getEmployeesRepository() {
        return employeeRepository;
    }


    public List<Employee> findAll() {
        return getEmployeesRepository().findAll();
    }

    public Employee findById(Long id) {
        return getEmployeesRepository().findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public void update(Long id, Employee employee) {
        Employee toBeUpdatedEmployee = findById(id);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        employeeRepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return getEmployeesRepository().findByGender(gender);
    }

    public Employee create(Employee employee) {
        return getEmployeesRepository().save(employee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return getEmployeesRepository().findAll(PageRequest.of(page - 1, size)).getContent();
    }

    public void delete(Long id) {
        getEmployeesRepository().deleteById(id);
    }
}
